package service;

import Model.Product;
import Model.PromotionItem;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static config.ProductInformation.APPLE_BAG;
import static config.ProductInformation.ORANGE;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class BasketServiceTest {


    private BasketService basketService = new BasketService();
    private List<String> items = Arrays.asList("apple", "orange", "oats", "apple");

    @Test
    public void buildShoppingCartProductTest() {
        ShoppingCart shoppingCart = basketService.buildShoppingCart(items);
        Product apple = shoppingCart.getProducts().stream().filter(i -> i.getName().equals("apple")).findAny().orElse(null);
        Product orange = shoppingCart.getProducts().stream().filter(i -> i.getName().equals("orange")).findAny().orElse(null);

        assert apple != null;
        assertEquals(apple.getName(), APPLE_BAG.getName());
        assertThat(apple.getQuantity(), is(2));
        assertThat(apple.getPrice(), is(APPLE_BAG.getPrice()));


        assertEquals(orange.getName(), ORANGE.getName());
        assertThat(orange.getQuantity(), is(1));
    }

    @Test
    public void buildShoppingCartPromotionTest() {
        ShoppingCart shoppingCart = basketService.buildShoppingCart(items);
        PromotionItem applePromotion = shoppingCart.getPromotionItems().stream().filter(i -> i.getPromotionInformation().getPromotionBasedProduct().getName().equals("apple")).findAny().orElse(null);

        assert applePromotion != null;
        assertThat(applePromotion, instanceOf(PromotionItem.class));
        assertThat(applePromotion.getPromotionInformation().getPromotionBasedProduct().getQuantity(), is(2));
    }
}
