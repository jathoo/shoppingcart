package service;

import Model.CartItem;
import Model.Product;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

import static config.ProductInformation.*;
import static junit.framework.TestCase.assertEquals;

public class SinglePromotionPaymentServiceTest {
    PaymentService paymentService = new PaymentService();

    @Test
    public void netPriceTest() {
        List<Product> products = Arrays.asList(new CartItem(1, ORANGE), new CartItem(1, APPLE_BAG));
        ShoppingCart shoppingCart = new ShoppingCart(products, Arrays.asList());

        BigDecimal totalPrice = paymentService.netPrice(shoppingCart);
        assertEquals(totalPrice, getBigDecimalValueOf(21.00));
    }

    @Test
    public void totalPriceTest() {
        List<Product> products = Arrays.asList(new CartItem(1, ORANGE), new CartItem(1, APPLE_BAG));
        ShoppingCart shoppingCart = new ShoppingCart(products, Arrays.asList());

        BigDecimal totalPrice = paymentService.totalPrice(shoppingCart);
        assertEquals(totalPrice, getBigDecimalValueOf(20.9));
    }

    @Test
    public void totalPriceTest1() {
        List<Product> products = Arrays.asList(new CartItem(2, BLUEBERRY), new CartItem(1, APPLE_BAG));
        ShoppingCart shoppingCart = new ShoppingCart(products, Arrays.asList());

        BigDecimal totalPrice = paymentService.totalPrice(shoppingCart);
        assertEquals(totalPrice, getBigDecimalValueOf(80.9));

    }

    @Test
    public void netPriceTest1() {
        List<Product> products = Arrays.asList(new CartItem(3, ORANGE), new CartItem(10, APPLE_BAG));
        ShoppingCart shoppingCart = new ShoppingCart(products, Arrays.asList());

        BigDecimal totalPrice = paymentService.netPrice(shoppingCart);
        assertEquals(totalPrice, getBigDecimalValueOf(70.00));

    }

    private BigDecimal getBigDecimalValueOf(Double value) {
        return BigDecimal.valueOf(value).setScale(2).setScale(2, RoundingMode.CEILING);
    }


}
