package service;

import Model.CartItem;
import org.junit.Test;

import static config.ProductInformation.APPLE_BAG;
import static config.ProductInformation.BLUEBERRY;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class DiscountServiceTest {

    private DiscountService discountService = new DiscountService();

    @Test
    public void verifyPromotionFalseTest() { assertFalse( discountService.verifyPromotion(new CartItem(2, BLUEBERRY))); }

    @Test
    public void verifyPromotionTrueTest() {
        assertTrue( discountService.verifyPromotion(new CartItem(2,APPLE_BAG)));
    }
}

