package service;

import Model.CartItem;
import Model.Product;
import Model.PromotionInformation;
import Model.PromotionItem;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static config.ProductInformation.*;
import static config.PromotionType.MULTI;
import static java.util.Arrays.asList;
import static junit.framework.TestCase.assertEquals;

public class MultiPromotionPaymentServiceTest {
    PaymentService paymentService = new PaymentService();

    @Test
    public void netPriceTest() {
        List<Product> products = asList(new CartItem(1, ORANGE), new CartItem(3, BLUEBERRY));
        ShoppingCart shoppingCart = new ShoppingCart(products, asList());

        BigDecimal netPrice = paymentService.netPrice(shoppingCart);
        assertEquals(netPrice, getBigDecimalValueOf(140.00));
    }

    @Test
    public void totalPriceTest() {
        List<Product> products = asList(new CartItem(3, SOUP),  new CartItem(3, BLUEBERRY));
        PromotionInformation promotionInformation = new PromotionInformation(new CartItem(SOUP),new CartItem(APPLE_BAG),4);
        List<PromotionItem> promotionItems = asList(new PromotionItem(promotionInformation, MULTI));

        ShoppingCart shoppingCart = new ShoppingCart(products, promotionItems);

        BigDecimal totalPrice = paymentService.totalPrice(shoppingCart);
        assertEquals(totalPrice, getBigDecimalValueOf(121.95));
    }

    @Test
    public void breadAndSoupTest() {
        List<Product> products = asList(new CartItem(2, SOUP),  new CartItem(1,BREAD));
        PromotionInformation promotionInformation = new PromotionInformation(new CartItem(2, SOUP),new CartItem(1,BREAD),2);
        List<PromotionItem> promotionItems = asList(new PromotionItem(promotionInformation,MULTI));

        ShoppingCart shoppingCart = new ShoppingCart(products,promotionItems);

        BigDecimal totalPrice = paymentService.totalPrice(shoppingCart);
        assertEquals( paymentService.netPrice(shoppingCart),getBigDecimalValueOf(2.10));
        assertEquals(totalPrice,getBigDecimalValueOf(1.71));

    }

    private BigDecimal getBigDecimalValueOf(Double value) {
        return BigDecimal.valueOf(value).setScale(2).setScale(2, RoundingMode.CEILING);
    }
}
