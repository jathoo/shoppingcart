package service;

import Model.Product;
import service.endpoints.IPaymentService;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PaymentService implements IPaymentService {

    private DiscountService discountService = new DiscountService();

    public BigDecimal netPrice(ShoppingCart shoppingCart) {
        return convertPriceToBigDecimal(shoppingCart,false);
    }

    public BigDecimal totalPrice(ShoppingCart shoppingCart) {
        BigDecimal finalPrice = convertPriceToBigDecimal(shoppingCart,true);
        discountService.printPriceInformation(shoppingCart);
        return finalPrice;
    }

    private BigDecimal convertPriceToBigDecimal(ShoppingCart shoppingCart, boolean totalPrice) {
       return  BigDecimal.valueOf(getPrice(shoppingCart, totalPrice)).setScale(2, RoundingMode.CEILING);
    }


    private Double getPrice(ShoppingCart shoppingCart, boolean totalPrice) {
        return shoppingCart.getProducts().stream()
                .mapToDouble(product -> calculatePrice(product, shoppingCart, totalPrice))
                .sum();
    }


    private double calculatePrice(Product product, ShoppingCart shoppingCart, Boolean totalPrice) {
        return discountService.applyPromotion(product, shoppingCart, totalPrice);
    }

}
