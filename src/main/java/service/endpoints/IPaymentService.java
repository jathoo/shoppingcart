package service.endpoints;

import service.ShoppingCart;

import java.math.BigDecimal;

public interface IPaymentService {
    BigDecimal netPrice(ShoppingCart shoppingCart);
    BigDecimal totalPrice(ShoppingCart shoppingCart);
}
