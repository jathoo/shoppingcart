package service.endpoints;

import java.util.List;

public interface IUserService {
    List<String> getUserInformation();
}
