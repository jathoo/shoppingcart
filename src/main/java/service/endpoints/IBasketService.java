package service.endpoints;

import service.ShoppingCart;

import java.util.List;

public interface IBasketService {
    ShoppingCart buildShoppingCart(List<String> productNames);
}
