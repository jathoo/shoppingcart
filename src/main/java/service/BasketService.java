package service;

import Model.Product;
import Model.PromotionInformation;
import Model.PromotionItem;
import config.Data.PromotionList;
import config.PromotionType;
import service.endpoints.IBasketService;
import utils.ProductFactory;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class BasketService implements IBasketService {

    private DiscountService discountService = new DiscountService();
    private PromotionList promotionList = new PromotionList();

    private Map productNamesAndQuantity;

    public ShoppingCart buildShoppingCart(List<String> productNames) {
        List<Product> products = buildBasketWith(productNamesAndQuantity(productNames));
        List<PromotionItem> promotionItems = buildOfferItems(products);
        return new ShoppingCart(products, promotionItems);
    }


    private List<Product> buildBasketWith(Map<String, Integer> productNamesAndQuantity) {
        setProductNamesAndQuantity(productNamesAndQuantity);
        return buildBasket(productNamesAndQuantity);
    }

    private void setProductNamesAndQuantity(Map<String, Integer> productNamesAndQuantity) {
        this.productNamesAndQuantity = productNamesAndQuantity;
    }


    private List<PromotionItem> buildOfferItems(List<Product> products) {
        return products.stream()
                .filter(product -> discountService.verifyPromotion(product))
                .map(this::createPromotionItemFor)
                .collect(Collectors.toList());

    }


    private static List<Product> buildBasket(Map<String, Integer> items) {
        return items.entrySet()
                .stream()
                .map(ProductFactory::apply)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private Map<String, Integer> productNamesAndQuantity(List<String> productNames) {
        return productNames.parallelStream().collect(Collectors.toConcurrentMap(product -> product, product -> 1, Integer::sum));
    }


    private PromotionItem createPromotionItemFor(Product product) {
        PromotionInformation promotionInformation = new PromotionInformation(getBasedPromotionProductFormPromotionList(product), product);
        return new PromotionItem(promotionInformation,getPromotionTypeFor(product));
    }

    private Product getBasedPromotionProductFormPromotionList(Product product) {
        return promotionList.PROMOTIONS.stream()
                .filter(promotionItem -> isProductOnPromotion(promotionItem,product))
                .map(i -> setQtyAndGetBasedProduct(i.getPromotionInformation().getPromotionBasedProduct()))
                .findAny()
                .orElse(product);
    }

    private PromotionType getPromotionTypeFor(Product product) {
        return promotionList.PROMOTIONS.stream()
                .filter(promotionItem-> isProductOnPromotion(promotionItem,product))
                .map(PromotionItem::getPromotionType)
                .findFirst()
                .orElse(null);
    }

    private boolean isProductOnPromotion(PromotionItem promotionItem, Product product){
       return promotionItem.getPromotionInformation().getProductOnPromotion().getName().equals(product.getName());
    }

    private Integer getBasedPromotionItemQty(String name) {
        return (Integer) productNamesAndQuantity.get(name);
    }

    private Product setQtyAndGetBasedProduct(Product baseProduct) {
        baseProduct.setQuantity(getBasedPromotionItemQty(baseProduct.getName()));
        return baseProduct;
    }


}
