package service;

import Model.Product;
import Model.PromotionItem;

import java.util.List;
import java.util.Optional;

public class ShoppingCart {

    private List<Product> products;
    private List<PromotionItem> promotionItems;

    public ShoppingCart(List<Product> products, List<PromotionItem> promotionItems) {
        this.products = products;
        this.promotionItems = promotionItems;
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<PromotionItem> getPromotionItems() {
        return promotionItems;
    }

    public Integer amountOfBaseProduct(String productName) {
        return promotionItems.stream()
                .filter(i -> i.getPromotionInformation().getPromotionBasedProduct().getName().equals(productName))
                .map(i -> getAvailableProductQty(i.getPromotionInformation().getAvailableProductQty()))
                .findFirst()
                .orElse(0);
    }

    public Optional<Product> productToDiscount(String productName) {
        return promotionItems.stream()
                .filter(i -> i.getPromotionInformation().getProductOnPromotion().getName().equals(productName))
                .map(i -> i.getPromotionInformation().getProductOnPromotion())
                .findAny();
    }

    private Integer getAvailableProductQty(Integer qty) {
        if (qty != null) {
            return qty;
        } else {
            return 0;
        }

    }


}
