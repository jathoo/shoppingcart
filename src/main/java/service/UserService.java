package service;

import service.endpoints.IUserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class UserService implements IUserService {

    public List<String> getUserInformation() {
        System.out.println("Please Enter items to price basket (Eg: soup soup bread ) ...");

        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        return new ArrayList<>(Arrays.asList(line.split(" ")));
    }
}
