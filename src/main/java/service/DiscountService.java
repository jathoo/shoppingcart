package service;

import Model.Product;
import Model.PromotionItem;
import config.Data.PromotionList;
import config.PromotionType;
import service.discount.DefaultPriceCalculator;
import service.discount.DiscountManager;
import service.discount.MultiPriceCalculator;
import service.discount.SinglePriceCalculator;
import utils.PromotionVerifier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static config.PromotionType.MULTI;
import static config.PromotionType.SINGLE;

public class DiscountService {

    private PromotionVerifier promotionVerifier = new PromotionVerifier();
    private ShoppingCart shoppingCart;

    boolean verifyPromotion(Product product) {
        return promotionVerifier.isInPromotion(product);
    }

    Double applyPromotion(Product product, ShoppingCart shoppingCart, Boolean totalPrice) {
        Double amount = 0.00;
        this.shoppingCart = shoppingCart;

        if (isInPromotion(product, totalPrice)) {
            if (getPromotionType(product) == SINGLE) {
                amount = applyDiscountToTotalPrice(new SinglePriceCalculator(product), amount, product);
            } else if (getPromotionType(product) == MULTI) {
                amount = applyDiscountToTotalPrice(new MultiPriceCalculator(product, shoppingCart), amount, product);
            }
        } else {
            amount = amount + new DiscountManager().processDiscount(new DefaultPriceCalculator(product));
        }
        return amount;
    }

    boolean isInPromotion(Product product, boolean totalPrice) {
        return verifyPromotion(product) && totalPrice && hasAPromotionType(product);
    }

    PromotionType getPromotionType(Product product) {
        PromotionList promotionList = new PromotionList();
        return promotionList.PROMOTIONS.stream()
                .filter(item -> isProductOnPromotion(item, product))
                .findFirst()
                .map(PromotionItem::getPromotionType)
                .orElse(null);
    }

    PromotionItem getPromotionItemFromShoppingCart(Product product) {
        return this.shoppingCart.getPromotionItems().stream()
                .filter(item -> isProductOnPromotion(item, product))
                .findFirst()
                .orElse(null);
    }

    private boolean isProductOnPromotion(PromotionItem promotionItem, Product product) {
        return promotionItem.getPromotionInformation().getProductOnPromotion().getName().equals(product.getName());
    }

    private void setNewPriceOnPromotion(PromotionItem promotionItem, BigDecimal offerPrice) {
        if (promotionItem != null) {
            promotionItem.getPromotionInformation().setNewPrice(offerPrice);
            if (offerPrice.compareTo(BigDecimal.ZERO) > 0) {
                promotionItem.getPromotionInformation().setPromotionApplied(true);
            }
        }
    }

    void printPriceInformation(ShoppingCart shoppingCart) {

        final ArrayList message = new ArrayList<>();
        if (!shoppingCart.getPromotionItems().isEmpty()) {
            shoppingCart.getPromotionItems().stream()
                    .filter(i -> i.getPromotionInformation().isPromotionApplied())
                    .peek(i -> message.add(i.getPromotionInformation().getProductOnPromotion().getName()))
                    .peek(i -> message.add(i.getPromotionType().getMessage()))
                    .peek(i -> message.add(i.getPromotionInformation().getNewPrice().toString()))
                    .map(i -> {
                        printInfo(String.join(" ", message));
                        return i;
                    })
                    .filter(i -> !i.getPromotionInformation().isPromotionApplied())
                    .peek(i -> printInfo("No offers available"))
                    .collect(Collectors.toList());
        } else {
            printInfo("No offers available");
        }
    }

    void printInfo(String message) {
        System.out.println(message);

    }

    private boolean hasAPromotionType(Product product) {
        return getPromotionType(product) != null;
    }

    private Double applyDiscountToTotalPrice(PriceCalculator priceCalculator, Double amount, Product product) {
        Double discountPrice = new DiscountManager().processDiscount(priceCalculator);
        setNewPriceOnPromotion(getPromotionItemFromShoppingCart(product), BigDecimal.valueOf(discountPrice));
        return amount + getFinalPrice(product, discountPrice);
    }

    private Double getFinalPrice(Product product, Double discountPrice) {
        return product.getPrice() - discountPrice;
    }


}
