package service.discount;

import service.PriceCalculator;

public class DiscountManager {


    public Double processDiscount(PriceCalculator priceCalculator){
        return priceCalculator.apply();
    }
}
