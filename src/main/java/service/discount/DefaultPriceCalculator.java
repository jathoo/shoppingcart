package service.discount;

import Model.Product;
import service.PriceCalculator;

public class DefaultPriceCalculator implements PriceCalculator {

    private final Product product;

    public DefaultPriceCalculator(Product product) {
        this.product = product;
    }

    @Override
    public Double apply() {
        return product.getPrice() * product.getQuantity();
    }
}
