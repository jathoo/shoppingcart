package service.discount;

import Model.Product;
import Model.PromotionItem;
import config.Data.PromotionList;
import service.PriceCalculator;
import service.ShoppingCart;

import java.util.Optional;

import static config.PromotionType.MULTI;

public class MultiPriceCalculator implements PriceCalculator {

    private final ShoppingCart shoppingCart;
    private final Product product;


    public MultiPriceCalculator(Product product, ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
        this.product = product;
    }

    @Override
    public Double apply() {
        return getPriceOfOfferItem();


    }

    Double getPriceOfOfferItem() {
        PromotionList promotionList = new PromotionList();
        return promotionList.PROMOTIONS.stream()
                .filter(this::isProductOnPromotion)
                .filter(promotionItem -> promotionItem.getPromotionInformation().getPromotionMinQty() <= promotionBasedItemQuantity(promotionItem))
                .map(promotionItem -> productToDiscount(getProductOnPromotionName(promotionItem)))
                .filter(Optional::isPresent)
                .mapToDouble(i -> getDiscountValue(i.get()))
                .findAny()
                .orElse(0.00);
    }

    private boolean isProductOnPromotion(PromotionItem promotionItem) {
        return getProductOnPromotionName(promotionItem).equals(product.getName());
    }

    private String getProductOnPromotionName(PromotionItem promotionItem) {
        return promotionItem.getPromotionInformation().getProductOnPromotion().getName();
    }

    private Integer promotionBasedItemQuantity(PromotionItem promotionItem) {
        String name = promotionItem.getPromotionInformation().getPromotionBasedProduct().getName();
        return shoppingCart.amountOfBaseProduct(name);
    }

    private Optional<Product> productToDiscount(String productName) {
        return shoppingCart.productToDiscount(productName);

    }

    private Double getDiscountValue(Product product) {
        return product.getPrice() * MULTI.getValue();
    }
}
