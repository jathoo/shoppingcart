package service.discount;

import Model.Product;
import service.PriceCalculator;

import static config.PromotionType.SINGLE;

public class SinglePriceCalculator implements PriceCalculator {

    private final Product product;

    public SinglePriceCalculator(Product product) {
        this.product = product;
    }

    @Override
    public Double apply() {
        return  (product.getPrice() * SINGLE.getValue()) * product.getQuantity();
     }
}
