package service;

public interface PriceCalculator {
    Double apply();
}
