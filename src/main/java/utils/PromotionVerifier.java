package utils;

import Model.Product;
import config.Data.PromotionList;

public class PromotionVerifier {

    public boolean isInPromotion(Product product) {
        PromotionList promotionList = new PromotionList();
       return promotionList.PROMOTIONS.stream().anyMatch(item -> item.getPromotionInformation().getProductOnPromotion().getName().equalsIgnoreCase(product.getName()));
    }
}
