package utils;

import Model.CartItem;
import Model.Product;

import java.util.Map;

import static config.ProductInformation.*;

public class ProductFactory {

    private static Product createProduct(String name, Integer qty) {

        if (APPLE_BAG.getName().equalsIgnoreCase(name)) return new CartItem(qty, APPLE_BAG);
        else if (ORANGE.getName().equalsIgnoreCase(name)) return new CartItem(qty, ORANGE);
        else if (SOUP.getName().equalsIgnoreCase(name)) return new CartItem(qty, SOUP);
        else if (BREAD.getName().equalsIgnoreCase(name)) return new CartItem(qty, BREAD);
        else if (MILK.getName().equalsIgnoreCase(name)) return new CartItem(qty, MILK);
        else return null;
    }


    public static Product apply(Map.Entry<String, Integer> itemMap) {
        return createProduct(itemMap.getKey(), itemMap.getValue());
    }
}
