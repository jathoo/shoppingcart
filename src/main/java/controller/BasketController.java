package controller;

import service.BasketService;
import service.endpoints.IBasketService;

import java.util.List;

public class BasketController {

    private PaymentController paymentController = new PaymentController();
    private IBasketService basketService = new BasketService();

    void processOrder(List<String> productNames){
         paymentController.calculateAndPrintPrice(basketService.buildShoppingCart(productNames));
    }


}
