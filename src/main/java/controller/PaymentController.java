package controller;

import service.PaymentService;
import service.ShoppingCart;
import service.endpoints.IPaymentService;

public class PaymentController {

private IPaymentService paymentService = new PaymentService();

    void calculateAndPrintPrice(ShoppingCart items) {
        System.out.println(" Subtotal : " + paymentService.netPrice(items));
        System.out.println(" Total :" + paymentService.totalPrice(items));
    }


}
