package controller;

import service.UserService;
import service.endpoints.IUserService;

class UserController {

    private BasketController basketController = new BasketController();
    private IUserService userService = new UserService();

    void checkOut(){
        basketController.processOrder(userService.getUserInformation());
    }

}
