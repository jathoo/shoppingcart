package Model;

import config.ProductInformation;

public abstract class Product {
    public Integer quantity = 0;
    public ProductInformation productDetails;


    public Product(ProductInformation productDetails) {
        this.productDetails = productDetails;
    }

    public Product(Integer quantity, ProductInformation productDetails) {
        this.quantity = quantity;
        this.productDetails = productDetails;
    }


    public abstract String getName();
    public abstract Integer getQuantity();
    public abstract void setQuantity(Integer quantity);
    public abstract Double getPrice();
}
