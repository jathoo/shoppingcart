package Model;

import config.ProductInformation;

public class CartItem extends Product {

    public CartItem(ProductInformation productDetails) {
        super(productDetails);
    }

    public CartItem(Integer qty, ProductInformation productDetails){
        super(qty,productDetails);
    }

    @Override
    public String getName() {
        return productDetails.getName();
    }

    @Override
    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;

    }

    @Override
    public Double getPrice() {
        return productDetails.getPrice();
    }
}
