package Model;

import config.PromotionType;

public class PromotionItem {
    private PromotionType promotionType;
    private PromotionInformation promotionInformation;

    public PromotionItem(PromotionInformation promotionInformation, PromotionType promotionType) {
        this.promotionType = promotionType;
        this.promotionInformation = promotionInformation;
    }


    public PromotionType getPromotionType() {
        return promotionType;
    }

    public PromotionInformation getPromotionInformation() {
        return promotionInformation;
    }

}
