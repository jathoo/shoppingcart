package Model;

import java.math.BigDecimal;

public class PromotionInformation {
    private Product promotionBasedProduct;
    private Product productOnPromotion;
    private Integer promotionMinQty;
    private Integer availableProductQty;
    private BigDecimal newPrice;
    private boolean promotionApplied = false;



    public PromotionInformation(Product promotionBasedProduct,Product productOnPromotion) {
        this.promotionBasedProduct = promotionBasedProduct;
        this.productOnPromotion = productOnPromotion;
        this.availableProductQty = promotionBasedProduct.getQuantity();
    }

     public PromotionInformation(Product promotionBasedProduct,Product productOnPromotion,Integer promotionMinQty) {
         this(promotionBasedProduct,productOnPromotion);
         this.promotionMinQty = promotionMinQty;
     }

    public Product getPromotionBasedProduct() {
        return promotionBasedProduct;
    }

    public Product getProductOnPromotion() {
        return productOnPromotion;
    }

    public Integer getPromotionMinQty() {
        return promotionMinQty;
    }

    public BigDecimal getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(BigDecimal newPrice) {
        this.newPrice = newPrice;
    }

    public Integer getAvailableProductQty() {
        return availableProductQty;
    }

    public boolean isPromotionApplied() {
        return promotionApplied;
    }

    public void setPromotionApplied(boolean promotionApplied) {
        this.promotionApplied = promotionApplied;
    }
}
