package config;

public enum PromotionType {
    SINGLE("10%",0.10),MULTI("50%",0.50);

    private String message;
    private Double value;

    PromotionType(String message,Double value) {
        this.message = message;
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public Double getValue() {
        return value;
    }
}
