package config;

public enum ProductInformation {
    APPLE_BAG("apple" , 1.00),
    ORANGE("orange" , 20.00),
    BLUEBERRY("berry",40.00),
    SOUP("soup" , 0.65),
    BREAD("bread",0.80),
    MILK("milk",1.30);

    private String name;
    private Double price;

    ProductInformation(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }
}
