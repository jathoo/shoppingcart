package config.Data;

import Model.CartItem;
import Model.Product;
import Model.PromotionInformation;
import Model.PromotionItem;

import java.util.Arrays;
import java.util.List;

import static config.ProductInformation.*;
import static config.PromotionType.MULTI;
import static config.PromotionType.SINGLE;

public class PromotionList {
    Product apple = new CartItem(APPLE_BAG);

    private PromotionInformation applePromotion = new PromotionInformation(apple,apple,1);
    private PromotionInformation orangePromotion= new PromotionInformation(new CartItem(BLUEBERRY), new CartItem(ORANGE),2);
    private PromotionInformation soupBreadPromotion = new PromotionInformation(new CartItem(SOUP),new CartItem(BREAD),2);


    public List<PromotionItem> PROMOTIONS = Arrays.asList(
            new PromotionItem(applePromotion, SINGLE),
            new PromotionItem(orangePromotion, MULTI),
            new PromotionItem(soupBreadPromotion, MULTI)
    );
}

