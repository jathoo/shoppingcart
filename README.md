## Prerequisites

1. gradle 
2. Java 8

Install gradle to run the project

This project is using Java 8

### running the application 

#### command prompt 
1. To run the application use 

./gradlew build  

./gradlew run 

2. Wait for this message to appear 
"Please Enter items to price basket (Eg: soup soup bread ) ..."

3. Enter product details (Eg:)
    ##tested with 
    apple
    soup soup bread
    milk
    
4. you should see the result. please note if you enter any products that's not in the shopping basket(created from product factory class)
program will ignore the product

#### IntelliJ
go to Initializer.class to run the application and use the program from intelliJ terminal

### to run all the test use 
./gradlew test

#### Troubleshooting 
if IntelliJ is missing the dependencies 
1. go to file
2.click re-import gradle project


#### Planned to do but could not implement.
CDI 2.0 with constructor Injection to avoid manual object initialisation and more decoupling
de couple printPriceInformation and print it from a controller level
using Lombok to remove boilerplate code 
continuous run on project
persistence data (db or json or in memory db) 
possibly More code refactoring (Price data Type)